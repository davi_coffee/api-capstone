var cors = require("cors");
const jsonServer = require("json-server");
const auth = require("json-server-auth");

const app = jsonServer.create();
const router = jsonServer.router("db.json");
const port = process.env.PORT || 3001;

app.db = router.db;

const rules = auth.rewriter({
  "/users*": "/660/users$1",
  "/exams/:id": "/660/users/:id",
  "/vaccines/:id": "/660/users/:id",
  "/consultations/:id": "/660/users/:id",
  // "/users/:type": "/660/users?type=:type",
  // "/users/delete/:id": "/660/users/:id",
});

app.use(cors());
app.use(rules);
app.use(auth);
app.use(router);
app.listen(port);

console.log("Server is running on port:", port);
